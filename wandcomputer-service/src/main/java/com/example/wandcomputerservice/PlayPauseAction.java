package com.example.wandcomputerservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class PlayPauseAction implements Action {

    private final MusiccastClient musiccastClient;

    public PlayPauseAction(MusiccastClient musiccastClient) {
        this.musiccastClient = musiccastClient;
    }

    @Override
    public String getName() {
        return "play_pause";
    }

    @Override
    public void execute() {
        try {
            String play;
            if (isPlaying()) {
                play = "pause";
            } else {
                play = "play";
            }
            String spec = "/YamahaExtendedControl/v1/netusb/setPlayback?playback=" + play;
            musiccastClient.makeRequest(spec);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isPlaying() {
        try {
            var s = musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/getPlayInfo", PlayInfo.class);
            return s.getPlayback().equals("play");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
