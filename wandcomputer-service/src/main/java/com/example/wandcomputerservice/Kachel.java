package com.example.wandcomputerservice;

public class Kachel {
    private final String title;
    private final String command;

    public Kachel(String title, String command) {
        this.title = title;
        this.command = command;
    }

    public String getTitle() {
        return title;
    }

    public String getCommand() {
        return command;
    }
}
