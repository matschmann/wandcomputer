package com.example.wandcomputerservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@Slf4j
@Component
public class MusiccastClient {

    public final HttpClient httpClient = HttpClient.newHttpClient();
    private final ObjectMapper objectMapper;
    private final MusiccastConfig musiccastConfig;

    public MusiccastClient(ObjectMapper objectMapper, MusiccastConfig musiccastConfig) {
        this.objectMapper = objectMapper;
        this.musiccastConfig = musiccastConfig;
    }

    public String makeRequest(String url) {
        List<MusiccastConfig.Device> deviceList = musiccastConfig.getDeviceList();
        assert !deviceList.isEmpty() : "There must be devices, but there aren't";
        String defaultDevice = musiccastConfig.getDefaultDevice();
        assert defaultDevice != null && !defaultDevice.isEmpty() : "No default device is given";
        var host = deviceList.stream().filter(o -> {
            return o.getName().equals(defaultDevice);
        }).findFirst().orElseThrow();

        try {
            URI uri = URI.create("http://"+host.getIp() + url);
            log.debug("Caling {}", uri);

            HttpRequest request = HttpRequest.newBuilder()
                    .uri(uri)
                    .build();
            byte[] bytes = httpClient.send(request, HttpResponse.BodyHandlers.ofString()).body().getBytes();
            return new String(bytes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T makeRequest(String url, Class<T> clazz) {
        try {
            var s = makeRequest(url);
            return objectMapper.readValue(s, clazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
