package com.example.wandcomputerservice;

import java.util.List;

public class InfoList {

    private Integer response_code;
    private String input;
    private Integer menu_layer;
    private Integer max_line;
    private Integer index;
    private Integer playing_index;
    private String menu_name;
    private List<ListInfo> list_info;

    public InfoList(Integer response_code, String input, Integer menu_layer, Integer max_line, Integer index, Integer playing_index, String menu_name, List<ListInfo> list_info) {
        this.response_code = response_code;
        this.input = input;
        this.menu_layer = menu_layer;
        this.max_line = max_line;
        this.index = index;
        this.playing_index = playing_index;
        this.menu_name = menu_name;
        this.list_info = list_info;
    }

    public Integer getResponse_code() {
        return response_code;
    }

    public String getInput() {
        return input;
    }

    public Integer getMenu_layer() {
        return menu_layer;
    }

    public Integer getMax_line() {
        return max_line;
    }

    public Integer getIndex() {
        return index;
    }

    public Integer getPlaying_index() {
        return playing_index;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public List<ListInfo> getList_info() {
        return list_info;
    }
}

class ListInfo {

    private String text;
    private Integer attribute;

    public String getText() {
        return text;
    }

    public Integer getAttribute() {
        return attribute;
    }
}
