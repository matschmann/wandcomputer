package com.example.wandcomputerservice;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class PlayInfo {
        private String input;
        private String play_queue_type;
        private String playback;
        private String repeat;
        private String shuffle;
        private String artist;
        private String album;
        private String track;
        private String albumart_url;
        private String usb_devicetype;
        private boolean auto_stopped;
        private Integer response_code;
        private Long play_time;
        private Long total_time;
        private Long albumart_id;
        private Long attribute;

}
