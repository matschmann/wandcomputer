package com.example.wandcomputerservice;

public interface Action {
    public String getName();

    void execute();
}
