package com.example.wandcomputerservice;

import org.springframework.stereotype.Component;

@Component
public class NextTrackAction implements Action {

    private final MusiccastClient musiccastClient;

    public NextTrackAction(MusiccastClient musiccastClient) {
        this.musiccastClient = musiccastClient;
    }

    @Override
    public String getName() {
        return "next_track";
    }

    @Override
    public void execute() {
        musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/setPlayback?playback=next");
    }
}
