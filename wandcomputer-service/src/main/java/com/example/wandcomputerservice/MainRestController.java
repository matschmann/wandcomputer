package com.example.wandcomputerservice;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//@CrossOrigin(origins = "http://192.168.178.36:8080/")
@RestController
public class MainRestController {

    private final NotImplementedAction notImplementedAction;
    private final ZufallAction zufallAction;
    private final NextTrackAction nextTrackAction;
    private final PreviousTrackAction previousTrackAction;
    private final PlayPauseAction playPauseAction;

    public MainRestController(PlayPauseAction playPauseAction, NotImplementedAction notImplementedAction, ZufallAction zufallAction, NextTrackAction nextTrackAction, PreviousTrackAction previousTrackAction) {
        this.playPauseAction = playPauseAction;
        this.notImplementedAction = notImplementedAction;
        this.zufallAction = zufallAction;
        this.nextTrackAction = nextTrackAction;
        this.previousTrackAction = previousTrackAction;
    }

    private List<Kachel> getTiles() {
        return List.of(
                new Kachel(playPauseAction.isPlaying() ? "Pause" : "Play", playPauseAction.getName()),
                new Kachel("Zufall", this.zufallAction.getName()),
                new Kachel("Previous", previousTrackAction.getName()),
                new Kachel("Next", nextTrackAction.getName()));
    }

    @GetMapping("/tiles")
    List<Kachel> listAllTiles() {
        return getTiles();
    }


    @GetMapping("/tiles/{action}")
    List<Kachel> run_action(@PathVariable("action") String actionstr) {

        Action action = getAction(actionstr);

        action.execute();
        try {
            Thread.sleep(100L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return getTiles();
    }

    private Action getAction(String actionstr) {
        switch (actionstr) {
            case "play_pause":
                return playPauseAction;
            case "next_track":
                return nextTrackAction;
            case "previous_track":
                return previousTrackAction;
            case "zufall":
                return zufallAction;
            default:
                return notImplementedAction;
        }

    }

}
