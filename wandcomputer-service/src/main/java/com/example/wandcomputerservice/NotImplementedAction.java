package com.example.wandcomputerservice;

import org.springframework.stereotype.Component;


@Component
public class NotImplementedAction implements Action {
    @Override
    public String getName() {
        return "not_implemented";
    }

    @Override
    public void execute() {
        //do nothing
    }
}
