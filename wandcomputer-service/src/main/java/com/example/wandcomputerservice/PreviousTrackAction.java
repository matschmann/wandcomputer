package com.example.wandcomputerservice;

import org.springframework.stereotype.Component;

@Component
public class PreviousTrackAction implements Action{

    private final MusiccastClient musiccastClient;

    public PreviousTrackAction(MusiccastClient musiccastClient) {
        this.musiccastClient = musiccastClient;
    }

    @Override
    public String getName() {
        return "previous_track";
    }

    @Override
    public void execute() {
        musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/setPlayback?playback=previous");
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/setPlayback?playback=previous");
    }
}
