package com.example.wandcomputerservice;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class ActionSerializer extends JsonSerializer<Action> {

    @Override
    public void serialize(Action action, JsonGenerator jgen, SerializerProvider serializerProvider) throws IOException {
        jgen.writeStartObject();
        jgen.writeStringField("name", action.getName());
        jgen.writeEndObject();
    }
}
