package com.example.wandcomputerservice;

import org.springframework.stereotype.Component;

@Component
public class ZufallAction implements Action {

    private final MusiccastClient musiccastClient;

    public ZufallAction(MusiccastClient musiccastClient) {
        this.musiccastClient = musiccastClient;
    }

    @Override
    public String getName() {
        return "zufall";
    }

    @Override
    public void execute() {
        InfoList infoList = musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/getListInfo?list_id=main&input=server&size=8&lang=de", InfoList.class);
        int listCounter;
        for (listCounter = 0; listCounter < infoList.getList_info().size(); listCounter++) {
            if (infoList.getList_info().get(listCounter).getText().equals("Rainer Pop")) {
                break;
            }
        }
        musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/setListControl?list_id=main&type=select&index="+listCounter+"&zone=main");
        musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/setListControl?list_id=main&type=select&index=2&zone=main");
        musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/setListControl?list_id=main&type=select&index=6&zone=main");
        musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/setListControl?list_id=main&type=play&index=1&zone=main");
        musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/setPlayback?playback=next");
//        musiccastClient.makeRequest("/YamahaExtendedControl/v1/netusb/toggleShuffle");

    }
}
