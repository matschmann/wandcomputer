package com.example.wandcomputerservice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@SpringBootTest
class WandcomputerServiceApplicationTests {


	private MockMvc mockMvc;
	private PlayPauseAction playPauseAction;
	private NotImplementedAction helloWorldAction;
	private ZufallAction zufallAction;
	private PreviousTrackAction previousTrackAction;
	private NextTrackAction nextTrackAction;

	@BeforeEach
	void setUp() {
		playPauseAction = Mockito.mock(PlayPauseAction.class);
		helloWorldAction = Mockito.mock(NotImplementedAction.class);
		zufallAction = Mockito.mock(ZufallAction.class);
		previousTrackAction = Mockito.mock(PreviousTrackAction.class);
		nextTrackAction = Mockito.mock(NextTrackAction.class);
		MainRestController mainRestController = new MainRestController(playPauseAction, helloWorldAction, zufallAction, nextTrackAction, previousTrackAction);
		mockMvc = standaloneSetup(mainRestController)
				.defaultRequest(get("/").accept(MediaType.APPLICATION_JSON))
				.alwaysExpect(status().isOk())
				.alwaysExpect(content().contentType("application/json"))
				.build();
	}

	@Test
    void root_tiles() throws Exception {
		mockMvc.perform(get("/tiles/"))
				.andExpect(jsonPath("$[0].title").value("Play"))
				.andExpect(jsonPath("$[1].title").value("Zufall"))
				.andExpect(jsonPath("$[2].title").value("Previous"))
				.andExpect(jsonPath("$[3].title").value("Next"));
    }

	@Test
	void play_pause() throws Exception {
		mockMvc.perform(get("/tiles/play_pause"));
		Mockito.verify(playPauseAction).execute();
	}
	@Test
	void zufall() throws Exception {
		mockMvc.perform(get("/tiles/zufall"));
		Mockito.verify(zufallAction).execute();
	}
	@Test
	void next_track() throws Exception {
		mockMvc.perform(get("/tiles/next_track"));
		Mockito.verify(nextTrackAction).execute();
	}
	@Test
	void previous_track() throws Exception {
		mockMvc.perform(get("/tiles/previous_track"));
		Mockito.verify(previousTrackAction).execute();
	}
}
